﻿using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class Build : MonoBehaviour {
    static string m_dst_path;

    [MenuItem("Build/Test")]
    public static void BuildTest() {
        var envDic = System.Environment.GetEnvironmentVariables();
        string str_version_name = Convert.ToString(envDic["VersionName"]);

        string project_path = Path.GetDirectoryName(Path.GetDirectoryName(Application.dataPath));
        m_dst_path = project_path + "/Test";

        CheckAndMakeDistDir(m_dst_path);

        PlayerSettings.bundleVersion = str_version_name;
        string m_error = BuildPipeline.BuildPlayer(GetEnabledScenes(), m_dst_path + ".exe", BuildTarget.StandaloneWindows64, BuildOptions.Development);

        if (!string.IsNullOrEmpty(m_error)) {
            return;
        }

        Debug.Log("Build Test");
    }

    static void CheckAndMakeDistDir(string dirPath, bool makeClean = false) {
        if (Directory.Exists(dirPath) == false) {
            Directory.CreateDirectory(dirPath);
            return;
        }

        if (makeClean) {
            DirectoryInfo rootDir = new DirectoryInfo(dirPath);
            foreach (FileInfo file in rootDir.GetFiles()) {
                file.Delete();
            }
            foreach (DirectoryInfo dir in rootDir.GetDirectories()) {
                dir.Delete(true);
            }
        }
    }

    private static string[] GetEnabledScenes() {
        List<string> l = new List<string>();

        foreach (EditorBuildSettingsScene ebss in EditorBuildSettings.scenes) {
            if (ebss.enabled) {
                l.Add(ebss.path);
            }
        }

        return l.ToArray();
    }
}
